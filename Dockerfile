FROM registry.gitlab.com/omiguelc/archlinux-env:master

# SETUP WORKDIR
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY setup setup
RUN mv setup/* . && rm -rf setup

# INSTALL
RUN bash step1.sh

# BUILD AUR PACKAGES
USER builder
WORKDIR /home/builder
RUN bash /usr/src/app/step2.sh

# CONFIGURE APPS
USER root
WORKDIR /usr/src/app
RUN bash step3.sh

# ENTRYPOINT
ENTRYPOINT ["/bin/entrypoint"]

