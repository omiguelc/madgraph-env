# UNPACK
cp profile.d/* /etc/profile.d
cp bin/entrypoint /bin
ls -lah /bin/entrypoint

# INSTALL PACKAGES
pacman -Syu --noconfirm
pacman -S --noconfirm --needed - < packages/packages.txt

# USERADD
useradd -m builder
