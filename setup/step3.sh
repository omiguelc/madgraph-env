source /etc/profile

# INSTALL AUR
find /home/builder -type f -name "*.pkg.tar*" 2>&1 | grep -v 'Permission denied' > packages.txt
pacman -U --noconfirm - < packages.txt

# CLEANUP
userdel builder && rm -rf /home/builder
sudo pacman -Scc --noconfirm && sudo pacman -Rns --noconfirm $(pacman -Qtdq) || echo 'Nothing to remove'

# SETUP MG5
mg5_aMC packages/mg5.txt

# CLEANUP
rm -rf /usr/src/app/*
